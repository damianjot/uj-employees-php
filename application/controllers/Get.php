<?php


class Get extends CI_Controller {

  private $sql = array() ; 
  private $limit = 1000;
  public function index (  )
	{
  }


  
  public function sapFromOrcid()
  {
    if( !empty( $_GET['orcidId'] ) ) :
     $O = trim( $_GET['orcidId'] ) ;
      $this->db->where( ' orcidid = ' ,  $O );
      $this->db->order_by( 'uid' );
      $this->db->limit( 1 );
      $SAP = $this->db->get_where( 'katalog._widok_orcid' )->result_array() ;
      echo $SAP ? json_encode ($SAP) : json_encode ( array( array( 'uid'=> null , 'orcidid' => $O ) ) ) ;
    else :
      die( 'Brak parametru "orcidId".' );
    endif;
  }
  
  


  public function orcidreader_data( )
	{




    if( !empty( $_GET['ujNumbers'] ) ) :

      // $UJID = preg_replace("/[A-Z]+/", "", strtoupper( $_GET['ujNumbers'] ) );
      $UJIDt =  strtoupper(trim( $_GET['ujNumbers'] ) ) ;
      $UJID = "'0'"; $UJIDt = explode( ',' , $UJIDt ) ; foreach( $UJIDt AS $u ) $UJID .= " , '{$u}' " ; // echo $UJID;
      $this->db->where( ' uid in ( '. $UJID .' )' );

     // $this->db->order_by( 1 ,'DESC' );
     // $this->db->limit( 1 );

      $ORCID = $this->db->get_where( 'katalog._widok_orcid' )->result_array() ;
       
      echo json_encode ($ORCID);

     
    else :
      die( 'Brak parametru "ujNumbers" - numery sap, usos przedzielone przecinkiem.' );
    endif;




  }



  public function ujPerson()
	{

    # limit z get 
    $this->limit = ( !empty( $_GET['limit'] ) && (int) $_GET['limit'] > 0  ) ? $_GET['limit'] : $this->limit ;

    # UJ SAP, USOS
    if( !empty( $_GET['ujNumber'] ) ) :
      $UJID = $_GET['ujNumber'] ; 
      $T  = preg_replace("/[^A-Z]+/", "", strtoupper($UJID) );
      $ID = preg_replace("/[^0-9]+/", "", $UJID ) ;
      if( ! in_array( $T , array( 'SAP' , 'USOS' ) ) ) die( 'Nieprawidłowy parametr' ) ;

      $this->db->limit( 1 , 0 ) ;
      $this->db->where( 'z = '  , $T );
      $this->db->where( 'nr =' , $ID );
    # UJ SAP, USOS - END ##############

     
    # NAZWISKO  
    elseif ( !empty( $_GET['lastName'] ) ) :
      $this->db->limit( $this->limit , 0 ) ;
      $this->db->where( " translate( trim ( unaccent ( lower(n) ) ), ',;\".'':[]|' , '' )  ILIKE translate( trim ( unaccent ( lower( '%".$_GET['lastName']."%' ) ) ), ',;\".'':[]|' , '' ) " );
    # NAZWISKO - END ##############

    # NAZWISKO i imie - filtr - ograniczanie
    elseif ( !empty( $_GET['fullName'] ) ) :
      # czyscimy...
      $czyscioch = trim(preg_replace('!\s+!', ' ', $_GET['fullName'] ));
      $this->db->limit( $this->limit , 0 ) ;      
      foreach ( explode( ' ' , $czyscioch ) AS $slowo ) :
        $this->db->where( " translate( trim ( unaccent ( lower(n)||' '||lower(i) ) ), ',;\".'':[]|' , '' ) ILIKE translate( trim ( unaccent ( lower( '%".$slowo."%' ) ) ), ',;\".'':[]|' , '' ) " );
      endforeach;
    # NAZWISKO - i imie - filtr - ograniczanie END ##############


    #brak - wypad  
    else :
      die('brak parametru wejściowego');
    endif ;


    # URUCHOM JSON
    $this->_daj_osoby () ;  

  }





  private function _daj_osoby ( )
	{
     
    $R = $this->db->get_where( 'katalog._widok_dymki' )->result() ;
  
    # wyslij
     echo isset( $R ) ? json_encode( $R , JSON_UNESCAPED_UNICODE )  : '[]' ;
   
  }

















  





  public function uj_person ( $UJID = '' )
	{
  
    #walidacja
    $T  = preg_replace("/[^a-z]+/", "", strtolower($UJID) );
    $ID = preg_replace("/[^0-9]+/", "", $UJID ) ;
    if( ! in_array( $T , array( 'sap' , 'usos' ) ) ) die( 'Nieprawidłowy parametr' ) ;

    # baza
    $this->db->limit( 1 );
    $R = $this->db->get_where( 'do_api.do_'.$T , array( 'nr' => $ID ) )->result() ;
  
    # wyslij
     echo isset( $R ) ? json_encode( $R )  : 'null' ;
   





  }















  public function record ( $HASH = '' )
	{

    if( ! $HASH ) die('brak parametru');



    $DO_IN = "'0' " ; 

     
    $this->db->select( ' uj_common_id ' ) ;
    $this->db->select( ' imie ' ) ;
    $this->db->select( ' nazwisko ' ) ;
    $this->db->select( ' data_start AS aktualizacja' ) ;
    $this->db->order_by( 'nazwisko' , 'ASC' ) ;
    $R = $this->db->get_where( 'do_api.osoba' , array( 'uj_common_id' => $HASH ) )->result() ;
    foreach( $R AS $r ) {
      $RR[ $r->uj_common_id] = (array) $r;
      $DO_IN .=  ", '" .$r->uj_common_id . "' " ;
    }
  
    $this->db->select( ' numer_typ , numer_id , uj_common_id ' ) ;
    $this->db->order_by( 'numer_typ' , 'ASC' ) ;
    $this->db->where( " uj_common_id IN ( " . $DO_IN . " ) " ) ;  
    $N = $this->db->get_where( 'do_api.osoba_numer' )->result() ;
 
 
 
    foreach( $N AS $n ) { 
 
      //print_r($n);

      # usos
      if( $n->numer_typ == 'usos_id' ) :
        echo 1;
        
      endif ;
        


      # sap
      if( $n->numer_typ == 'sap_id' ) :
        echo 2 ;
        
      endif ;
      
      
                
       # orcid
       if( $n->numer_typ == 'orcid_id' ) :
        echo 3 ;
        
      endif ;
 


    }

    
    // $RRR = array_values($N);

    /// print_r($RRR) ;
    // echo json_encode( $RRR );
    //echo $DO_IN ;







  }




























}
