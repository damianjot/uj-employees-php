<?php



	$query_builder = true ;
	$db_debug = true ;
	$active_group = 'katalog';



	// katalog - baza główna
	$db['katalog'] = array(
		'dsn'	=> '',
		'hostname' => '127.0.0.1',
		'username' => 'katalog',
		'password' => 'kata4log!',
		'port'     => '5433',
		'database' => 'katalog',
		'dbdriver' => 'postgre',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => $db_debug,
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);
	

	// baza ruj - postgres
	$db['baza_ruj'] = array(
		'dsn'	=> '',
		'hostname' => '127.0.0.1',
		'username' => 'dspace',
		'password' => 'd$pace1234',
		'port'     => '5434',
		'database' => 'dspace_ruj',
		'dbdriver' => 'postgre',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => $db_debug,
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
  );


// baza usos - oracle
$db['oracle_usos'] = array(
    'dsn'   => '',
    'hostname' => 'tarvos.uoks.uj.edu.pl:1521/USOS',
    'username' => 'BJ_DOKTORANCI', 
    'password' => 'ZYd9195IswPJSTrV', 
    'database' => 'db_name',
		'dbdriver' => 'oci8',
		'port'     => '1521',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => $db_debug,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);