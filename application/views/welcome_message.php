<?php


 exit;



?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Api - Doktoranci USOS</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;v
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>




<div id="container">
	<h1>Api - Doktoranci USOS</h1>

	<div id="body">
	<p>&nbsp;</p>

		<p>Autoryzacja:
		<ul>
		  <li>Z adresacji UJ - z autoryzowanych adresów IP</li>
		  <li>Z pozostałych adresów - po vpn z autoryzowanych IP - po podaniu klucza api</li>
		</ul>
		</p>
		<code>Parametr uri: key/xxxkluczxxx</code>

		<p>&nbsp;</p>
		<p>Pobieranie danych z bazy Oracle:</p>
		<code>http://149.156.74.43/get/from_oracle
		<br>Pobiera nowe dane z bazy Oracle i zapisuje do bazy api.
		<br>Zwraca info o ilości nowych/zaktualizowanych rekordów:
		<br>&lt;tmp_usos&gt;
		<br>&nbsp;&nbsp;&lt;date_time&gt; yyyy-mm-dd hh:mm:ss &lt;/date_time&gt;
		<br>&nbsp;&nbsp;&lt;new_records&gt; integer &lt;/new_records&gt;
		<br>&nbsp;&nbsp;&lt;update_records&gt; integer &lt;/update_records&gt;
		<br>&lt;/tmp_usos&gt;
		</code>

		<p>&nbsp;</p>
		<p>Sprawdzenie czy są nowe dane:</p>
		<code>http://149.156.74.43/check/new - (bez parametru - komplet rekordów)
		<br>http://149.156.74.43/check/new/date/yyyy-mm-dd/time/hh:mm - ("time" nieobowiazkowe, brak "date" ignoruje "time")
		<br>Zwraca liczbę nowych i zaktualizowanych rekordów nowszych od daty podanej w parametrach:
		<br>&lt;usos_check&gt;
		<br>&nbsp;&nbsp;&lt;new_records&gt; integer &lt;/new_records&gt;
		<br>&nbsp;&nbsp;&lt;update_records&gt; integer &lt;/update_records&gt;
		<br>&lt;/usos_check&gt;
		</code>

		<p>&nbsp;</p>
		<p>Pobiera dane w formacie xml:</p>
		<code>http://149.156.74.43/get/new/ - (bez parametru - komplet rekordów)
		<br>http://149.156.74.43/get/new/date/yyyy-mm-dd/time/hh:mm - ("time" nieobowiazkowe, brak "date" ignoruje "time")
		<br>Zzwraca rekordy nowsze od daty podanej w parametrach:
		<br>&lt;Dokument XML&gt;
		</code>

		<p>&nbsp;</p>
		<p>Wyszukiwanie rekordów:</p>
		<code>http://149.156.74.43/get/search/ - (bez parametru - komplet rekordów)
		<br>http://149.156.74.43/get/search/name/string - ( imie nazwisko - lower(ascii) )
		<br>http://149.156.74.43/get/search/nr_usos/integer - ( USOS ID )
		<br>http://149.156.74.43/get/search/ruj_uid/hash - ( Posolony, zaszyfrowany PESEL )
		<br>Zwraca rekordy pasujące do parametrów wyszukiwania:
		<br>&lt;Dokument XML&gt;
		</code>


		<p></p>
	</div>

	<p class="footer">
	<!-- {elapsed_time} -->
	</p>
</div>

</body>
</html>